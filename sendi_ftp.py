#!/usr/bin/python3
# -*- coding: utf-8 -*-

from ftplib import FTP
from getpass import getpass
import generi_bi
import sys

def troviBildojn(artikolojListo):
    rezulto = []
    for artikolo in artikolojListo:
        for tipo, kol_1, kol_2 in artikolo:
            # Bedaŭrinde ĉi tio ne trovas bildojn ene de alineoj
            #print('Mi trovis "%s"-n en bel_informas.txt' % tipo)
            if tipo == 'BILDO':
                #print('Nomo de la bildo:', kol_1)
                rezulto.append(kol_1)
            elif tipo == 'BILDO_LIGILO':
                bildo, _ = kol_1
                #print('Nomo de la bildo:', bildo)
                rezult.append(bildo)
    return rezulto

def sendiIndexHtml(ftp, htmlNomo):
    print('Mi sendos %s kiel index.html' % htmlNomo)

    with open(htmlNomo, 'rb') as htmlDosiero:
        ftp.storlines('STOR ' + htmlNomo, htmlDosiero)

    if 'index.html' in ftp.nlst():
        i = 1
        while True:
            nomoDeSekurKopio = str(i) + '.' + 'index.html'
            if nomoDeSekurKopio not in ftp.nlst():
                print('La ekzistantan index.html mi renomos', nomoDeSekurKopio)
                ftp.rename('index.html', nomoDeSekurKopio)
                break
            else:
                i += 1
                if i == 1000:
                    # Por sekureco kontraŭ senfinaj cikloj
                    raise Exception('Ne eblis trovi nomon por sekurkopii la ekzistantan dosieron')
    print('Nun mi renomos %s en index.html' % htmlNomo)
    ftp.rename(htmlNomo, 'index.html')

def sendiBildojn(ftp, bildoj):
    for bildNomo in bildoj:
        with open(bildNomo, 'rb') as bildDosiero:
            if bildNomo not in ftp.nlst():
                print('Mi sendos', bildNomo)
                ftp.storbinary('STOR ' + bildNomo, bildDosiero)
            else:
                print(bildNomo, 'jam ekzistas, mi ne sendos ĝin denove')


artikolojListo, numero, dato = generi_bi.generiBelInformas()

dosierujo = '%d-%02d-%02d' % dato

print('La paĝo troviĝos ĉe: http://esperanto.com.br/bel/', dosierujo, sep = '')

bildoj = troviBildojn(artikolojListo)

ftp = FTP('ftp.esperanto.com.br')
try:
    #ftp.set_debuglevel(1)

    uzanto = 'esperantocom@briko.com.br'
    pasv = getpass('Pasvorto de %s: ' % uzanto)

    ftp.login(uzanto, pasv)

    ftp.cwd('bel')

    if dosierujo not in ftp.nlst():
        ftp.mkd(dosierujo)

    ftp.cwd(dosierujo)

    if len(sys.argv) >= 2:
        htmlNomo = sys.argv[1]
        sendiIndexHtml(ftp, htmlNomo)
    else:
        # Ĉu ni devas alvoki belinformas ĉi tie kaj generi novan HTML-on tute aŭtomate?
        print('Neniu HTML-dosiero informita sur la komand-linio. Mi sendos nur la bildojn trovitajn en bel_informas.txt.')

    sendiBildojn(ftp, bildoj)
finally:
    ftp.quit()

