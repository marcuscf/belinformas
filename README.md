# Legu min - belinformas.py #

Ĉi tio estas simpla programo por generi novajn eldonojn de "BEL Informas", laŭ la ekzistanta strukturo.

Ĝi estas verkita en la programlingvo Python (dekomence 2.7, sed 3.4+ ekde 2017-09-27), kiu ĝenerale jam estas instalita en Linux, kaj estas elŝutebla kaj facile instalebla en Windows. La biblioteko PIL (Python Imaging - Pillow) estas uzata por ĝustigi bildojn.

La programo funkcias jene:

1. Metu la enhavon en la dosiero `bel_informas.txt`. Sekvu la ekzemplon de formato de antaŭaj eldonoj de BEL Informas. Resume, la strukturo estas la jena:

    1. Metu la numeron de BEL Informas per la jena sintakso: `%!numero: 8`

    1. Metu la daton tiumaniere: `%!dato: 2016-12-31`. La dato estos uzata en la adreso de bildoj ankaŭ.

    1. Komencu, finu kaj apartigu artikolojn per `====================` (20 aŭ pli egalsignoj).

    1. Apartigu alineojn per 1 malplena linio.

    1. La unua alineo de ĉiu artikolo estas ties titolo.

    1. Uzu `--------------------` por apartigi la unua disde la dua kolumno de ĉiu artikolo.

    1. Uzu `[http://ioajn.example.com]`, `[ioajn.jpg]`, aŭ `[ioajn.jpg http://ioajn.example.com]` por ligiloj, bildoj, aŭ bildoj kun ligiloj.

    1. Uzu la formaton `**grasa teksto**` por emfazi vortojn, aŭ uzu HTML-on mem: `<i>klinita teksto</i>`, `<b>grasa teksto</b>`.

    1. Finu alineon per `||` por indiki ke ĝi devas aperi en ambaŭ kolumnoj.

    1. La programo ĝenerale postulas tiun formaton, do malgrandaj misaj detaloj povas provoki grandan diferencon en la rezulto. Tio okazas ĉar ĝenerale la nura uzanto de la programo estas ties aŭtoro :-)

    1. Se iu parto de la teksto enhavas iksojn anstataŭ verajn Esperantajn literojn (en la kodo UTF-8), vi povas marki tion per `%!konvertiiksojn: jes` sur linio antaŭ la konvertota teksto kaj `%!konvertiiksojn: ne` sur linio post tio.

1. Metu la bildojn en la sama dosierujo.

1. Rulu la programon `./belinformas.py`

1. Ĝi informos kiujn dosierojn ĝi kreis (HTML-on kaj bildojn), kaj vi povos kontroli la rezulton. La programo klopodas ĉiam krei novajn dosierojn kaj ne viŝas ekzistantajn. Probable vi preferos ŝanĝi la nomojn de la kreitaj dosieroj.

1. Do se necese, redaktu kaj rulu la programon denove por korekti erarojn aŭ ŝanĝi nomon de bildoj. Rimarku ke la bildoj ligos al http://www.esperanto.com.br, do ili nur aperos ĝuste post kiam vi sendos ilin al la servilo http://www.esperanto.com.br (kaj kun la ĝustaj nomoj kaj en la ĝusta dosierujo laŭ la dato de BEL Informas).

1. Sendu la paĝon per (ekz.) `./sendi_ftp.py 1.bel_informas.html` aŭ uzu simple `./sendi_ftp.py` por sendi la bildojn. La programo trovas la bildojn kiuj aperas solaj (ekz. `[bildo.jpg]||`) en `bel_informas.txt` kaj sendas ĝin al la servilo.

Nunaj avantaĝoj:

1. La strukturo de kolumnoj kaj la strukturo de artikoloj (titolo, enhavo, aŭtoro) estos ĉiam ĝustaj. Longaj ligiloj aperos sur pluraj linioj por ke la larĝeco de kolumnoj ne ŝanĝiĝu.

1. Oni bezonas tajpi informojn (numero, adreso de bildoj) en nur unu loko kaj ili aperos en ĉiuj necesaj lokoj.

1. La bildoj estos malgrandigitaj aŭtomate (la malgrandigitaj bildoj ĉiam estos en la formato `*.jpg` kaj je la ĝusta larĝeco), do BEL Informas ne enhavos bildojn je pluraj megabajtoj nek bildojn kiuj estas tro larĝaj por la ekrano.

1. Eblas elekti partojn de la teksto por konverti iksojn aŭtomate al la ĝustaj Esperantaj literoj.

1. La sendado de dosieroj per FTP estas pli facila.

Dezirataj estontaj avantaĝoj:

1. Esti uzebla de neinformadikuloj.
