#!/usr/bin/python3
# -*- coding: utf-8 -*-

import codecs
import os
import shutil
from contextlib import contextmanager
from enum import Enum

class DosierTipo(Enum):
    UNIKODO = 1
    SIMPLAJ_BAJTOJ = 2

@contextmanager
def novaRezultDosiero(dosierNomBazo, reĝimo):
    encoding = "utf-8"
    dosiero = None
    try:
        i = 1
        while True:
            rezultDosierNomo = str(i) + '.' + dosierNomBazo
            if not os.path.exists(rezultDosierNomo):
                try:
                    flags = os.O_WRONLY | os.O_CREAT | os.O_EXCL
                    if hasattr(os, 'O_BINARY'):
                        flags |= os.O_BINARY
                    fdDosiero = os.open(rezultDosierNomo, flags)
                except OSError as e:
                    # Ne eblis malfermi la dosieron. Ni provos alian nomon
                    continue
                if reĝimo is DosierTipo.UNIKODO:
                    dosiero = open(fdDosiero, "w", encoding = 'utf-8')
                    yield (rezultDosierNomo, dosiero)
                    break
                elif reĝimo is DosierTipo.SIMPLAJ_BAJTOJ:
                    dosiero = open(fdDosiero, "wb")
                    yield (rezultDosierNomo, dosiero)
                    break
                else:
                    raise Exception('Interna eraro')
            i += 1
            if i == 1000:
                # Por sekureco kontraŭ senfinaj cikloj
                raise Exception('Ne eblis trovi nomon por nova dosiero')
    finally:
        if dosiero is not None:
            dosiero.close()

# Ne uzata ĉar post kelkaj testoj mi decidis krei novan dosieron kun nova nomo anstataŭ kopii la originalan al nova nomo.
#def sekurKopii(dosierNomo):
#    with open(dosierNomo, 'rb') as originDosiero, novaRezultDosiero(dosierNomo, DosierTipo.SIMPLAJ_BAJTOJ) as (novaNomo, rezultDosiero):
#        print('Ŝanĝota dosiero', dosierNomo, 'kopiita al', novaNomo, 'por sekureco')
#        shutil.copyfileobj(originDosiero, rezultDosiero)

