#!/usr/bin/python3
# -*- coding: utf-8 -*-

from string import Template
import generi_bi
from nova_dosiero import novaRezultDosiero, DosierTipo

# ================================== STRUKTURO/ŜABLONOJ ==================================

apartiga_linio = r'''<tr>
      <td dir="ltr" align="left" bgcolor="#008080" height="22" valign="top" width="6">&nbsp;</td>
      <td dir="ltr" align="left" bgcolor="#008080" height="22" valign="top" width="286">&nbsp;</td>
      <td dir="ltr" align="left" bgcolor="#008080" height="22" valign="top" width="59">&nbsp;</td>
      <td dir="ltr" align="left" bgcolor="#008080" height="22" valign="top" width="290">&nbsp;</td>
      <td dir="ltr" align="left" bgcolor="#008080" height="22" valign="top" width="5">&nbsp;</td>
    </tr>'''

alinea_spaco = r'''<tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="286" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="59" height="22" align="left" valign="top" dir="ltr">&nbsp;</td>
      <td width="290" height="22" align="left" valign="top" dir="ltr">&nbsp;</td>
      <td width="5" height="22" align="left" valign="top" dir="ltr">&nbsp;</td>
    </tr>'''

larĝa_alinea_spaco = r'''<tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="286" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="59" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="290" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="5" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
    </tr>'''

ĝenerala_ŝablono = Template(r'''
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>BEL Informas</title>
<style>
    a {
        font-size: 90%;
    }
</style>
</head>

<body style="font-family: Verdana; font-size: 12 pt">

<div align="center">
  <center>
  <table border="3" style="border-collapse: collapse" bordercolor="#111111" width="637" id="AutoNumber2" bgcolor="#008080" cellspacing="6">
    <tr>
      <td width="630">
  <p align="center">
  <img border="0" src="http://www.esperanto.com.br/bel/informas.png" width="600" height="120"></p>
  <div align="center">
    <center>
  <table border="0" style="border-collapse: collapse" bordercolor="#111111" width="532" id="AutoNumber1" height="355" bgcolor="#FFFFFF">
    <tr>
      <td width="6" height="1" align="right" valign="top" dir="ltr" bgcolor="#C6FFE2">&nbsp;</td>
      <td width="286" height="1" align="right" valign="top" dir="ltr" bgcolor="#C6FFE2">
      <p align="left"><i style="font-size: small">
      <b>n-ro ${numero}/${jaro}</b></i></td>
      <td width="59" height="1" align="right" valign="top" dir="ltr">&nbsp;</td>
      <td width="290" height="1" align="right" valign="top" dir="ltr">
      &nbsp;</td>
      <td width="5" height="1" align="right" valign="top" dir="ltr">&nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="1" align="right" valign="top" dir="ltr" bgcolor="#C6FFE2">&nbsp;</td>
      <td width="286" height="1" align="right" valign="top" dir="ltr" bgcolor="#C6FFE2">
      <i style="font-size: small">la ${tago}an de ${monato_eo} ${jaro}</i></td>
      <td width="59" height="1" align="right" valign="top" dir="ltr">&nbsp;</td>
      <td width="290" height="1" align="right" valign="top" dir="ltr">
      <i style="font-size: small">${tago} de ${monato_pt} de ${jaro}</i></td>
      <td width="5" height="1" align="right" valign="top" dir="ltr">&nbsp;</td>
    </tr>
    ${apartiga_linio}
    ${artikoloj}
    <tr>
      <td width="6" height="22" align="center" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
      <td width="286" height="22" align="center" valign="top" bgcolor="#C6FFE2">
      <b>Ali&#285;u al niaj komunumoj ĉe Facebook kaj Google +</b></td>
      <td width="59" height="22" align="center" valign="top">&nbsp;</td>
      <td width="290" height="22" align="center" valign="top">
      <b>Conecte-se a nossas comunidades no Facebook e Google +</b></td>
      <td width="5" height="22" align="center" valign="top">
      &nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="22" align="center" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
      <td width="286" height="22" align="center" valign="top" bgcolor="#C6FFE2">
      <a target="_blank" href="http://www.facebook.com/groups/esperanto.basico.aprimoramento/">
      <img border="0" src="http://www.esperanto.com.br/bel/facebook.jpg" width="114" height="113" alt="Facebook"></a></td>
      <td width="59" height="22" align="center" valign="top">&nbsp;</td>
      <td width="290" height="22" align="center" valign="top">
      <a target="_blank" href="https://plus.google.com/communities/103944900190508342646">
      <img border="0" src="http://www.esperanto.com.br/bel/googleplus.jpg" width="113" height="113" alt="Google Plus"></a></td>
      <td width="5" height="22" align="center" valign="top">
      &nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
      <td width="286" height="22" align="left" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
      <td width="59" height="22" align="left" valign="top">&nbsp;</td>
      <td width="290" height="22" align="left" valign="top">
      &nbsp;</td>
      <td width="5" height="22" align="left" valign="top">
      &nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
      <td width="286" height="22" align="left" valign="top" bgcolor="#C6FFE2">
      <p align="center"><b>BEL invitas: vizitu kaj ŝatu nian paĝon ĉe facebook!</b></td>
      <td width="59" height="22" align="left" valign="top">&nbsp;</td>
      <td width="290" height="22" align="left" valign="top">
      <p align="center"><b>BEL convida: visite e curta nossa página no face!</b></td>
      <td width="5" height="22" align="left" valign="top">
      &nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="22" align="center" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
      <td width="286" height="22" align="center" valign="top" bgcolor="#C6FFE2">
      <a target="_blank" href="https://pt-br.facebook.com/brazilaesperantoligo">
      <img border="0" src="http://www.esperanto.com.br/bel/shati.jpg" width="292" height="139"></a></td>
      <td width="59" height="22" align="center" valign="top">&nbsp;</td>
      <td width="290" height="22" align="center" valign="top">
      <a target="_blank" href="https://pt-br.facebook.com/brazilaesperantoligo">
      <img border="0" src="http://www.esperanto.com.br/bel/curtir.jpg" width="292" height="138"></a></td>
      <td width="5" height="22" align="center" valign="top">
      &nbsp;</td>
    </tr>
    <tr>
      <td width="616" height="22" align="left" valign="top" bgcolor="#C6FFE2" colspan="5">
      <p align="center">
      <b style="font-size: large">
      <a target="_blank" href="http://www.facebook.com/brazilaesperantoligo">
      www.facebook.com/brazilaesperantoligo</a></b></td>
    </tr>
    ${apartiga_linio}
    <tr>
      <td width="616" height="22" align="left" valign="top" colspan="5">
      <p align="center">&nbsp;</p>
      <p align="center">
      <a target="_blank" href="http://belabutiko.esperanto.org.br/abonservo/index.php/adesao/ades-o-renovac-o-bel.html">
      <img border="0" src="http://www.esperanto.com.br/bel/filie-se.png" alt="Você já se refiliou à BEL?" width="525" height="138"></a></td>
    </tr>
    <tr>
      <td width="616" height="22" align="left" valign="top" colspan="5">
      <p align="center">&nbsp;</p>
      <p align="center">
      <a target="_blank" href="http://belabutiko.esperanto.org.br/">
      <img border="0" src="http://www.esperanto.com.br/bel/banner_belabutiko_1.gif" width="468" height="60" alt="FOTO Libroservo"></a></td>
    </tr>
    ${butiko}
    <tr>
      <td width="616" height="22" align="left" valign="top" bgcolor="#008080" colspan="5">
      <p align="center"><b style="font-size: xx-large; color: white">Ni laboru kune!</b></td>
    </tr>
    <tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#008080">
      &nbsp;</td>
      <td width="585" height="22" align="left" valign="top" bgcolor="#008080" colspan="3">
      &nbsp;</td>
      <td width="5" height="22" align="left" valign="top" bgcolor="#008080">
      &nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
      <td width="585" height="22" align="left" valign="top" bgcolor="#C6FFE2" colspan="3">
          <p style="font-size: small">
          <b>Boletim bilíngue da Liga Brasileira de Esperanto</b>
          <br>
          <b>Dulingva bulteno de Brazila Esperanto-Ligo</b></p>
        <p style="font-size: small">SDS Ed. Venâncio III Sala 303, Brasília - DF, CEP 
        70.393-902
        <br>Telefones: (61) 3226-1298&nbsp; Fax: (61) 3226-4446
        </p>
        <p style="font-size: small"><b>Kunlaborantoj</b>:
        Querino Neto, Marcus Aurelius Farias, Leandro Silvestre, Paulo Nascentes, Fernando Maia.</p>
      </td>
      <td width="5" height="22" align="left" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
      <td width="585" height="22" align="left" valign="top" bgcolor="#C6FFE2" colspan="3">
      &nbsp;</td>
      <td width="5" height="22" align="left" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
    </tr><tr>
      <td dir="ltr" align="left" bgcolor="#008080" height="22" valign="top" width="6">&nbsp;</td>
      <td dir="ltr" align="left" bgcolor="#008080" height="22" valign="top" width="286">&nbsp;</td>
      <td dir="ltr" align="left" bgcolor="#008080" height="22" valign="top" width="59">&nbsp;</td>
      <td dir="ltr" align="left" bgcolor="#008080" height="22" valign="top" width="290">&nbsp;</td>
      <td dir="ltr" align="left" bgcolor="#008080" height="22" valign="top" width="5">&nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="22" align="center" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="286" height="22" align="center" valign="top" bgcolor="#C6FFE2" dir="ltr">
      <b>Sendu viajn novaĵojn al BEL informas</b></td>
      <td width="59" height="22" align="center" valign="top" dir="ltr">&nbsp;</td>
      <td width="290" height="22" align="center" valign="top" dir="ltr">
      <b>Envie as suas notícias para o BEL informas</b></td>
      <td width="5" height="22" align="center" valign="top" dir="ltr">&nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="286" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">
        Ĉu vi deziras kontribui por la dulingva informilo de BEL kaj BEJO sendante novaĵojn?
        Se jes, bonvole plenigu la formularon per la necesaj datumoj:
      </td>
      <td width="59" height="22" align="left" valign="top" dir="ltr">&nbsp;</td>
      <td width="290" height="22" align="left" valign="top" dir="ltr">
        Você deseja contribuir com o boletim bilíngue da BEL e da BEJO, enviando novidades?
        Se sim, por favor preencha o formulário com as informações necessárias:
      </td>
      <td width="5" height="22" align="left" valign="top" dir="ltr">&nbsp;</td>
    </tr><tr>
      <td width="616" height="22" align="center" valign="top" bgcolor="#C6FFE2" dir="ltr" colspan="5">
        <a target="_blank" href="http://goo.gl/forms/TGVFgwhlOR">
          <img border="0" src="http://www.esperanto.com.br/bel/tablet.jpg" width="246" height="162">
        </a>
      </td>
    </tr><tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="585" height="22" align="center" valign="top" bgcolor="#C6FFE2" dir="ltr" colspan="3">
         <b style="font-size: 110%">
          <a target="_blank" href="http://goo.gl/forms/TGVFgwhlOR">Sendu viajn novaĵojn</a>
        </b>
      </td>
      <td width="5" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
    </tr><tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="585" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr" colspan="3">&nbsp;</td>
      <td width="5" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2"> &nbsp;</td>
      <td width="585" height="44" align="center" valign="top" bgcolor="#C6FFE2" colspan="3" rowspan="2">
        <a target="_blank" href="https://docs.google.com/forms/d/1BY_8HR_kpeXPzpi_YsU3gA9_UCEDGDJQZj5xrb3j4WU/viewform">
        <img border="0" src="http://www.esperanto.com.br/bel/email.gif" align="middle" alt="FOTO Email" width="170" height="170"></a>
        <br>
        <strong style="font-size: small">
          <a data-cke-saved-href="https://docs.google.com/forms/d/1hQxbvZoUbsMSVzB0hcci2WLsnSqggX3ZPeqSxZ1XNj8/viewform" target="_blank" href="https://docs.google.com/forms/d/1BY_8HR_kpeXPzpi_YsU3gA9_UCEDGDJQZj5xrb3j4WU/viewform">Ricevu BEL Informas per via retadreso</a></strong>
        <br>
        <strong style="font-size: small">
          <a data-cke-saved-href="https://docs.google.com/forms/d/1hQxbvZoUbsMSVzB0hcci2WLsnSqggX3ZPeqSxZ1XNj8/viewform" target="_blank" href="https://docs.google.com/forms/d/1BY_8HR_kpeXPzpi_YsU3gA9_UCEDGDJQZj5xrb3j4WU/viewform">Receba BEL Informas em seu email</a></strong>
      </td>
      <td width="5" height="22" align="left" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
      <td width="5" height="22" align="left" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
      <td width="585" height="22" align="left" valign="top" bgcolor="#C6FFE2" colspan="3">
      &nbsp;<p align="center"><b style="font-size: x-large">
      <a href="http://www.esperanto.com.br/bel/index.htm">www.esperanto.com.br/bel/index.htm</a></b></p>
      <p align="center">&nbsp;</td>
      <td width="5" height="22" align="left" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
    </tr>
    </table>

    </center>
    </div>
      </td>
    </tr>
  </table>
  </center>
</div>
<p>&nbsp;</p>
<hr>
</body>
</html>''')

alineo_ŝablono = Template(r'''<tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="286" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">
        ${teksto_eo}
      </td>
      <td width="59" height="22" align="left" valign="top" dir="ltr">&nbsp;</td>
      <td width="290" height="22" align="left" valign="top" dir="ltr">
        ${teksto_pt}
      </td>
      <td width="5" height="22" align="left" valign="top" dir="ltr">&nbsp;</td>
    </tr>''')

larĝa_alineo_ŝablono = Template(r'''<tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="635" colspan="3" height="22" align="center" valign="top" bgcolor="#C6FFE2" dir="ltr">
        ${teksto}
      </td>
      <td width="5" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
    </tr>''')

bildo_ŝablono = Template(r'''<tr>
      <td width="616" height="22" align="center" valign="top" bgcolor="#C6FFE2" dir="ltr" colspan="5">
      <img border="0" src="${bildo_adreso}" width="${largxeco}" height="${alteco}"></td>
    </tr>''')

bildo_ligilo_ŝablono = Template(r'''<tr>
      <td width="616" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr" colspan="5">
      <p align="center">
      <a target="_blank" href="${ligilo_adreso}"><img border="0" src="${bildo_adreso}" width="${largxeco}" height="${alteco}"></a>
      </p></td>
    </tr>''')

titolo_ŝablono = Template(r'''<tr>
      <td width="6" height="22" align="center" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="286" height="22" align="center" valign="top" bgcolor="#C6FFE2" dir="ltr">
      <b>${teksto_eo}</b></td>
      <td width="59" height="22" align="center" valign="top" dir="ltr">&nbsp;</td>
      <td width="290" height="22" align="center" valign="top" dir="ltr">
      <b>${teksto_pt}</b></td>
      <td width="5" height="22" align="center" valign="top" dir="ltr">&nbsp;</td>
    </tr>''')

aŭtoro_ŝablono = Template(r'''<tr>
      <td width="6" height="22" align="center" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="286" height="22" align="center" valign="top" bgcolor="#C6FFE2" dir="ltr">
      <i style="font-size: small">${teksto_eo}</i></td>
      <td width="59" height="22" align="center" valign="top" dir="ltr">&nbsp;</td>
      <td width="290" height="22" align="center" valign="top" dir="ltr">
      <i style="font-size: small">${teksto_pt}</i></td>
      <td width="5" height="22" align="center" valign="top" dir="ltr">&nbsp;</td>
    </tr>''')

butiko_ŝablono = Template(r'''<tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
      <td width="286" height="22" align="left" valign="top" bgcolor="#C6FFE2">
      <b>Malkovru en via BEL@Butiko</b></td>
      <td width="59" height="22" align="left" valign="top">&nbsp;</td>
      <td width="290" height="22" align="left" valign="top">
      <b>Descubra em sua BEL@Butiko</b></td>
      <td width="5" height="22" align="left" valign="top">
      &nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="286" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="59" height="22" align="left" valign="top" dir="ltr">&nbsp;</td>
      <td width="290" height="22" align="left" valign="top" dir="ltr">&nbsp;</td>
      <td width="5" height="22" align="left" valign="top" dir="ltr">&nbsp;</td>
    </tr>
    ${butiko_enhavo}
    <tr>
      <td width="616" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr" colspan="5">
      <p align="center"><img border="0" src="http://www.esperanto.com.br/bel/rabato-10.jpg" width="583" height="310"></td>
    </tr>
    <tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="286" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">
AL BEL-MEMBROJ: 
POR RICEVI 10%-AN RABATON, PETU VIAN RABAT-KODON ĈE LA ADRESO:
      </td>
      <td width="59" height="22" align="left" valign="top" dir="ltr">&nbsp;</td>
      <td width="290" height="22" align="left" valign="top" dir="ltr">
AOS MEMBROS DA BEL:
PARA TER DESCONTO DE 10% NA COMPRA DE LIVROS, SOLICITE SEU CÓDIGO JUNTO AO ENDEREÇO:
      </td>
      <td width="5" height="22" align="left" valign="top" dir="ltr">&nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="286" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">
      &nbsp;</td>
      <td width="59" height="22" align="left" valign="top" dir="ltr">&nbsp;</td>
      <td width="290" height="22" align="left" valign="top" dir="ltr">
      &nbsp;</td>
      <td width="5" height="22" align="left" valign="top" dir="ltr">&nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="286" height="22" align="center" valign="top" bgcolor="#C6FFE2" dir="ltr">
      <p><b><a href="mailto:belabutiko@esperanto.org.br">
      belabutiko@esperanto.org.br</a>&nbsp; </b>
      <p>&nbsp;</td>
      <td width="59" height="22" align="left" valign="top" dir="ltr">&nbsp;</td>
      <td width="290" height="22" align="center" valign="top" dir="ltr">
      <p><b><a href="mailto:belabutiko@esperanto.org.br">
      belabutiko@esperanto.org.br</a>&nbsp; </b>
      <p>&nbsp;</td>
      <td width="5" height="22" align="left" valign="top" dir="ltr">&nbsp;</td>
    </tr>
    <tr>
      <td width="6" height="22" align="center" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
      <td width="286" height="22" align="center" valign="top" bgcolor="#C6FFE2">
      <b>Vizitu vian virtualan butikon:</b></td>
      <td width="59" height="22" align="center" valign="top">&nbsp;</td>
      <td width="290" height="22" align="center" valign="top">
      <b>Visite a sua loja virtual:</b></td>
      <td width="5" height="22" align="center" valign="top">
      &nbsp;</td>
    </tr>
    <tr>
      <td width="616" height="22" align="center" valign="top" bgcolor="#C6FFE2" colspan="5" dir="ltr">
      <b>
      <a href="http://www.belabutiko.esperanto.org.br">www.belabutiko.esperanto.org.br</a>
      </b>
      </td>
    </tr>
    <tr>
      <td width="6" height="22" align="left" valign="top" bgcolor="#C6FFE2">
      &nbsp;</td>
      <td width="610" height="22" align="center" valign="top" bgcolor="#C6FFE2" colspan="4">
        <p style="font-size: small">
          Francisco Mattos
          <br>
          <a href="mailto:belabutiko@esperanto.org.br">
          belabutiko@esperanto.org.br</a>
        </p>
      </td>
    </tr>''')

butiko_enhavo_ŝablono = Template(r'''<tr>
      <td width="6" height="22" align="center" valign="top" bgcolor="#C6FFE2" dir="ltr">&nbsp;</td>
      <td width="286" height="22" align="center" valign="top" bgcolor="#C6FFE2" dir="ltr">
        ${teksto_a}
      </td>
      <td width="59" height="22" align="center" valign="top" dir="ltr">&nbsp;</td>
      <td width="290" height="22" align="center" valign="top" dir="ltr">
        ${teksto_b}
      </td>
      <td width="5" height="22" align="center" valign="top" dir="ltr">&nbsp;</td>
    </tr>''')

# ================================== LOGIKO/KONSTRUADO ==================================
artikoloj_listo, numero, dato = generi_bi.generiBelInformas()
jaro, monato, tago = dato

baza_adreso = 'http://www.esperanto.com.br/bel/%d-%02d-%02d/' % dato

if baza_adreso and baza_adreso[-1] != '/':
    baza_adreso += '/'

def fari_artikolojn(artikoloj_listo):
    artikoloj_html_listo = []
    butiko_html_listo = []
    nuna_listo = artikoloj_html_listo
    ĝenerala_arg = {'baza_adreso': baza_adreso}
    for artikolo in artikoloj_listo:
        for tipo, kol_1, kol_2 in artikolo:
            if tipo == 'BILDO':
                larĝeco, alteco = kol_2
                nuna_listo.append(bildo_ŝablono.safe_substitute({
                    'bildo_adreso': baza_adreso + kol_1,
                    'largxeco': larĝeco,
                    'alteco': alteco
                }))
            elif tipo == 'BILDO_LIGILO':
                bildo, ligilo = kol_1
                larĝeco, alteco = kol_2
                if not ligilo.startswith('http://') and not ligilo.startswith('https://'):
                    ligilo = baza_adreso + ligilo
                nuna_listo.append(bildo_ligilo_ŝablono.safe_substitute({
                    'bildo_adreso': baza_adreso + bildo,
                    'ligilo_adreso': ligilo,
                    'largxeco': larĝeco,
                    'alteco': alteco
                }))
            elif tipo == 'TITOLO':
                if kol_1.strip() == 'BUTIKO': # La titolo 'BUTIKO' estas speciala
                    nuna_listo = butiko_html_listo
                else:
                    nuna_listo = artikoloj_html_listo
                    kol_1 = Template(kol_1).safe_substitute(ĝenerala_arg)
                    kol_2 = Template(kol_2).safe_substitute(ĝenerala_arg)
                    nuna_listo.append(titolo_ŝablono.safe_substitute({'teksto_eo': kol_1, 'teksto_pt': kol_2}))
                    #nuna_listo.append(alinea_spaco)
            elif tipo == 'ALINEO':
                kol_1 = Template(kol_1).safe_substitute(ĝenerala_arg)
                kol_2 = Template(kol_2).safe_substitute(ĝenerala_arg)
                if nuna_listo is artikoloj_html_listo:
                    alineo = alineo_ŝablono.safe_substitute({'teksto_eo': kol_1, 'teksto_pt': kol_2})
                elif nuna_listo is butiko_html_listo:
                    alineo = butiko_enhavo_ŝablono.safe_substitute({'teksto_a': kol_1, 'teksto_b': kol_2})
                nuna_listo.append(alineo)
                nuna_listo.append(alinea_spaco)
            elif tipo == 'LARĜA_ALINEO':
                if kol_1 is not None:
                    enhavo = kol_1
                else:
                    enhavo = kol_2
                enhavo = Template(enhavo).safe_substitute(ĝenerala_arg)
                nuna_listo.append(larĝa_alineo_ŝablono.safe_substitute({'teksto': enhavo}))
                nuna_listo.append(larĝa_alinea_spaco)
            elif tipo == 'AŬTORO':
                nuna_listo.append(aŭtoro_ŝablono.safe_substitute({'teksto_eo': kol_1, 'teksto_pt': kol_2}))
        nuna_listo.append(apartiga_linio)
        #nuna_listo.append(alinea_spaco)

    return ''.join(artikoloj_html_listo), ''.join(butiko_html_listo)

artikoloj, butiko = fari_artikolojn(artikoloj_listo)

monatoj_eo = ['', 'januaro', 'februaro', 'marto', 'aprilo', 'majo', 'junio',
           'julio', 'aŭgusto', 'septembro', 'oktobro', 'novembro', 'decembro']
monatoj_pt = ['', 'janeiro', 'fevereiro', 'março', 'abril', 'maio', 'junho',
           'julho', 'agosto', 'setembro', 'outubro', 'novembro', 'dezembro']

rezulta_html = ĝenerala_ŝablono.safe_substitute({
    'numero': numero, 'tago': tago,
    'monato_eo': monatoj_eo[monato], 'monato_pt': monatoj_pt[monato],
    'jaro': jaro,
    'baza_adreso': baza_adreso,
    'artikoloj': artikoloj,
    'butiko': butiko_ŝablono.safe_substitute({'butiko_enhavo': butiko}) if butiko else '',
    'apartiga_linio': apartiga_linio,
    'alinea_spaco': alinea_spaco
})

dosier_nomo = 'bel_informas.html'
with novaRezultDosiero(dosier_nomo, DosierTipo.UNIKODO) as (rezult_dosier_nomo, rezult_dosiero):
    rezult_dosiero.write(rezulta_html)
    print('Mi skribis la rezulton en', rezult_dosier_nomo)
