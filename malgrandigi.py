#!/usr/bin/python3
# -*- coding: utf-8 -*-

w = int(input('width: '))
h = int(input('height: '))

i = 95
while i > 0:
    rw = w * (i/100)
    rh = h * (i/100)
    print(i, '%: (', rw, ', ', rh, ')\twidth="', rw, '" height="', rh, '" \t    style="width: ', rw, 'px; height: ', rh, 'px"', sep = '')
    i -= 5
