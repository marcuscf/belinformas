#!/usr/bin/python3
# -*- coding: utf-8 -*-

import re
import os
import sys
import math
from enum import Enum
from nova_dosiero import novaRezultDosiero, DosierTipo
from PIL import Image

# Bildoj
MAKS_BILD_LARĜECO_1_KOL = 286
MAKS_BILD_LARĜECO_2_KOL = 440
MAKS_GRANDECO_KiB = 180

# Ligiloj
MAKS_LIGILO = 32


def ĉuAlineaApartigilo(linio):
    """Apartigilo de alineoj. Kiam aperas malplena linio, oni konsideras ĝin kiel apartigilon de alineoj."""
    return linio == '\n'


def ĉuKolumnApartigilo(linio):
    """Apartigilo de kolumnoj. Kiam aperas --------------------, la programo komprenas ke finiĝis la
    unua kolumno kaj komenciĝos la dua kolumno de la sama artikolo."""
    return ĉuApartigilo(linio, '-')


def ĉuArtikolApartigilo(linio):
    """Apartigilo de artikoloj. Kiam aperas ====================, la programo komprenas ke finiĝis la
    artikolo kaj komenciĝos alia artikolo"""
    return ĉuApartigilo(linio, '=')


def ĉuApartigilo(linio, ch):
    GRANDECO_DE_LINIO = 20
    rezulto = linio.startswith(GRANDECO_DE_LINIO * ch)
    if rezulto:
        for c in linio[GRANDECO_DE_LINIO:].strip():
            if c != ch:
                return False
    return rezulto


def ĉuLarĝaAlineo(teksto):
    """Kiam alineo finiĝas per tiu markilo: ||, la programo komprenas ke ĝi devos okupi ambaŭ kolumnojn.
    Ĉi tiaj alineoj povas aperi ĉu en la unua kolumno, ĉu en la dua. La rezulto estos egala, ĝi aperos
    kiel unu larĝa kolumno en la rezulto."""
    return teksto.strip().endswith('||')


def ĉuBildo(s):
    """Por meti ligilojn kaj bildojn, oni uzas krampojn: []. Ĉi tiu funkcio distingas tiujn kazojn
    per la finaĵo de la teksto. Se ĝi estas konata finaĵo de bildoj, oni konsideros ĝin kiel bildon."""
    return s.endswith('.png') or s.endswith('.jpg') or s.endswith('.gif')


def ĉuAtributo(s):
    return s.startswith('%!')


def legiAtributon(s):
    nomo, valoro = s[2:].split(':')
    nomo = nomo.strip().lower()
    valoro = valoro.strip().lower()
    if nomo == 'numero':
        valoro = int(valoro)
    elif nomo == 'dato':
        dato = valoro.split('-')
        valoro = tuple(int(d) for d in dato)
    elif nomo == 'konvertiiksojn':
        if valoro == 'jes':
            valoro = True
        elif valoro == 'ne':
            valoro = False
        else:
            valoro = None
    return nomo, valoro


def forigiLarĝAlineMarkilon(teksto):
    return teksto.strip()[:-2]


ligiloRe = re.compile(r'\[(.*?)\]')
nurBildoRe = re.compile(r'^\s*\[(.*?)\]\s*$')
duPartojRe = re.compile(r'(.+)\s+(\S+)$')
boldRe = re.compile(r'\*\*([^\s](|.*?[^\s])\**)\*\*') # kopiita de txt2tags, farita de Aurelio Jargas


def rompiLinion(s):
    if len(s) < MAKS_LIGILO:
        return s
    else:
        return s[0:MAKS_LIGILO] + '<br>' + rompiLinion(s[MAKS_LIGILO:])


def forigiMailto(s):
    mailto = 'mailto:'
    if s.startswith(mailto):
        return s[len(mailto):]
    else:
        return s


def transformiLinion(lin):
    """Ĉi tiu funkcio faras plurajn transformojn en linio, ekz.:
    - [http://ligiloj.example.com]
    - **grasa teksto**
    """
    def substLigilon(match):
        t = match.group(1)
        duPartoj = duPartojRe.match(t)
        if duPartoj:
            if ĉuBildo(duPartoj.group(1)):
                return '[%s]' % t # sen ŝanĝo
            else:
                ligTeksto = rompiLinion(duPartoj.group(1))
                return '<a target="_blank" href="%s">%s</a>' % (duPartoj.group(2), ligTeksto)
        else:
            if ĉuBildo(t):
                return '[%s]' % t # sen ŝanĝo
            else:
                ligTeksto = forigiMailto(t)
                ligTeksto = rompiLinion(ligTeksto)
                return '<a target="_blank" href="%s">%s</a>' % (t, ligTeksto)
    lin = ligiloRe.sub(substLigilon, lin)
    lin = boldRe.sub(r'<b>\1</b>', lin)
    return lin


def transformiBildojn(s):
    """Ĉi tiu funkcio transformas [bildon.jpg] en HTML-a bildo."""
    def subst(match):
        t = match.group(1)
        duPartoj = duPartojRe.match(t)
        if duPartoj: # Bildo kaj ligilo
            if ĉuBildo(duPartoj.group(1)):
                bildNomo = duPartoj.group(1)
                bildNomo = malgrandigiBildDosieron(bildNomo, MAKS_BILD_LARĜECO_2_KOL, MAKS_GRANDECO_KiB)
                grandeco = bildGrandeco(bildNomo, MAKS_BILD_LARĜECO_1_KOL)
                return '<a target="_blank" href="%s"><img src="${baza_adreso}%s" width="%d" height="%d"></a>' % (duPartoj.group(2), bildNomo, grandeco[0], grandeco[1])
            else:
                return '[%s]' % t # sen ŝanĝo
        else:
            if ĉuBildo(t):
                bildNomo = malgrandigiBildDosieron(t, MAKS_BILD_LARĜECO_2_KOL, MAKS_GRANDECO_KiB)
                grandeco = bildGrandeco(t, MAKS_BILD_LARĜECO_1_KOL)
                return '<img src="${baza_adreso}%s" width="%d" height="%d">' % (bildNomo, grandeco[0], grandeco[1])
            else:
                return '[%s]' % t # sen ŝanĝo
    return ligiloRe.sub(subst, s)


def nurBildo(s):
    """Ĉi tiu funkcio kontrolas ĉu alineo enhavas bildon kaj nenion pli."""
    match = nurBildoRe.match(s)
    if match:
        t = match.group(1)
        duPartoj = duPartojRe.match(t)
        if duPartoj:
            t = duPartoj.group(1)
        return ĉuBildo(t)
    return False


def nurBildEnhavo(s):
    match = nurBildoRe.match(s)
    t = match.group(1)
    return t


def malgrandigiBildDosieron(bildNomo, maksLarĝecoPx, maksGrandecoKiB):
    """Ĉi tiu funkcio testos ĉu bildo estas pli granda (kvanto de bajtoj) ol
    la dezirata grandeco. Se jes, la dosiero de la bildo estos malgrandigita
    (larĝeco kaj alteco) kaj la formato estos ŝanĝita al JPG por fari ĝin
    malpli peza en interreto. La nova bildo estos konservita kun nova dosiernomo.
    Tio bedaŭrinde povas generi multajn novajn dosierojn kiam oni rulas
    ĉi tiun programon multfoje. Tamen, oni certas ke oni ne perdos dosierojn akcidente.
    (Atentu: oni perdos kelkajn atributojn de PNG, kiel travidebleco)."""
    st = os.stat(bildNomo)
    bildoKiB = st.st_size / 1024
    if bildoKiB > maksGrandecoKiB:
        #sekurKopii(bildNomo)
        im = Image.open(bildNomo)
        novaGrandecoPx = kalkuliNovanGrandecon(im, maksLarĝecoPx)
        if bildNomo.endswith('.jpg'):
            dosierNomBazo = bildNomo
        elif bildNomo.endswith('.png') or bildNomo.endswith('.gif'):
            dosierNomBazo = bildNomo[:-4] + '.jpg'
        else:
            dosierNomBazo = bildNomo + '.jpg'
        with novaRezultDosiero(dosierNomBazo, DosierTipo.SIMPLAJ_BAJTOJ) as (novaBildNomo, novaDosiero):
            im.thumbnail(novaGrandecoPx, Image.ANTIALIAS)
            im.save(novaDosiero, "JPEG")
            print('Nova dosiero kreita:', novaBildNomo, ' (%.2f' % (os.stat(novaBildNomo).st_size / 1024), 'KiB). La originala estas: %s (%.2f KiB)' % (bildNomo, bildoKiB))
            return novaBildNomo
    else:
        return bildNomo


def kalkuliNovanGrandecon(image, maksBildLarĝecoPx):
    sz = image.size
    if sz[0] > maksBildLarĝecoPx:
        r = sz[0] / maksBildLarĝecoPx
        w = maksBildLarĝecoPx # estas egala al sz[0] / r
        h = int(math.ceil(sz[1] / r))
        return w, h
    else:
        return sz


def bildGrandeco(bildNomo, maksBildLarĝecoPx):
    """Ĉi tiu funkcio informas la grandecon de bildo. Se ĝi estas pli larĝa ol
    la larĝeco difinita en maksBildLarĝecoPx, ĉi tiu funkcio proporcie malgrandigos
    la rezulton. La dosiero ne estos ŝanĝita, do se la bildo okupas tro da KB aŭ MB,
    estos bone malgrandigi ĝin antaŭe."""
    im = Image.open(bildNomo)
    return kalkuliNovanGrandecon(im, maksBildLarĝecoPx)


def maliksigi(teksto):
    # ATENTU: zorgu pri ligiloj kiuj enhavas cx, ux, ktp. Uzu cxx, uxx, ktp.
    teksto = re.sub('CX(?!X)', 'Ĉ', teksto)
    teksto = re.sub('GX(?!X)', 'Ĝ', teksto)
    teksto = re.sub('HX(?!X)', 'Ĥ', teksto)
    teksto = re.sub('JX(?!X)', 'Ĵ', teksto)
    teksto = re.sub('SX(?!X)', 'Ŝ', teksto)
    teksto = re.sub('UX(?!X)', 'Ŭ', teksto)
    teksto = re.sub('Cx(?!x)', 'Ĉ', teksto)
    teksto = re.sub('Gx(?!x)', 'Ĝ', teksto)
    teksto = re.sub('Hx(?!x)', 'Ĥ', teksto)
    teksto = re.sub('Jx(?!x)', 'Ĵ', teksto)
    teksto = re.sub('Sx(?!x)', 'Ŝ', teksto)
    teksto = re.sub('Ux(?!x)', 'Ŭ', teksto)
    teksto = re.sub('cx(?!x)', 'ĉ', teksto)
    teksto = re.sub('gx(?!x)', 'ĝ', teksto)
    teksto = re.sub('hx(?!x)', 'ĥ', teksto)
    teksto = re.sub('jx(?!x)', 'ĵ', teksto)
    teksto = re.sub('sx(?!x)', 'ŝ', teksto)
    teksto = re.sub('ux(?!x)', 'ŭ', teksto)

    teksto = re.sub('CXX', 'CX', teksto)
    teksto = re.sub('GXX', 'GX', teksto)
    teksto = re.sub('HXX', 'HX', teksto)
    teksto = re.sub('JXX', 'JX', teksto)
    teksto = re.sub('SXX', 'SX', teksto)
    teksto = re.sub('UXX', 'UX', teksto)
    teksto = re.sub('Cxx', 'Cx', teksto)
    teksto = re.sub('Gxx', 'Gx', teksto)
    teksto = re.sub('Hxx', 'Hx', teksto)
    teksto = re.sub('Jxx', 'Jx', teksto)
    teksto = re.sub('Sxx', 'Sx', teksto)
    teksto = re.sub('Uxx', 'Ux', teksto)
    teksto = re.sub('cxx', 'cx', teksto)
    teksto = re.sub('gxx', 'gx', teksto)
    teksto = re.sub('hxx', 'hx', teksto)
    teksto = re.sub('jxx', 'jx', teksto)
    teksto = re.sub('sxx', 'sx', teksto)
    teksto = re.sub('uxx', 'ux', teksto)
    return teksto


def muntiAlineon(linioj):
    """Ĉi tiu funkcio prenas liston de linioj, transformas tiujn liniojn
    per transformiLinion, kaj kunigas ĉiujn en unu literĉenon."""
    transformitajLinioj = [transformiLinion(lin) for lin in linioj]
    return ''.join(transformitajLinioj);


def muntiArtikolon(alineojKol1, alineojKol2, larĝaj):
    """Ĉi tiu funkcio prenas listojn de alineoj kaj muntas artikolon en strukturon,
    kiun alia programo povos transformi en HTML-on."""

    rezulto = []
    rezulto.append(('TITOLO', alineojKol1[0], alineojKol2[0]))

    kunAŭtoro = False # ni ne uzas aŭtorojn plu.
    limoj = slice(1, None)
    #if alineojKol1[0].strip() == 'BUTIKO':
    #    kunAŭtoro = False
    #    limoj = slice(1, None)
    #else:
    #    kunAŭtoro = True
    #    limoj = slice(1, -1)

    for a, b in zip(alineojKol1[limoj], alineojKol2[limoj]):
        a = transformiBildojn(a)
        b = transformiBildojn(b)
        rezulto.append(('ALINEO', a, b))

    if kunAŭtoro:
        if alineojKol1[-1] or alineojKol2[-1]:
            rezulto.append(('AŬTORO', alineojKol1[-1], alineojKol2[-1]))

    for poz, teksto in larĝaj[::-1]:
        if nurBildo(teksto):
            bildTekstEnhavo = nurBildEnhavo(teksto)
            duPartoj = duPartojRe.match(bildTekstEnhavo)
            if duPartoj:
                bildNomo = duPartoj.group(1)
                bildLigilo = duPartoj.group(2)
                bildNomo = malgrandigiBildDosieron(bildNomo, MAKS_BILD_LARĜECO_2_KOL, MAKS_GRANDECO_KiB)
                rezulto.insert(poz, ('BILDO_LIGILO', (bildNomo, bildLigilo), bildGrandeco(bildNomo, MAKS_BILD_LARĜECO_2_KOL)))
            else:
                bildNomo = bildTekstEnhavo
                bildNomo = malgrandigiBildDosieron(bildNomo, MAKS_BILD_LARĜECO_2_KOL, MAKS_GRANDECO_KiB)
                rezulto.insert(poz, ('BILDO', bildNomo, bildGrandeco(bildNomo, MAKS_BILD_LARĜECO_2_KOL)))
        else:
            teksto = transformiBildojn(teksto)
            rezulto.insert(poz, ('LARĜA_ALINEO', teksto, None))

    return rezulto


def aldoniAlineon(listoNormalaj, larĝaj, alineo):
    if not alineo:
        return
    muntitaAlineo = muntiAlineon(alineo)
    if ĉuLarĝaAlineo(muntitaAlineo):
        poz = len(listoNormalaj)
        muntitaAlineo = forigiLarĝAlineMarkilon(muntitaAlineo)
        larĝaj.append((poz, muntitaAlineo))
    else:
        listoNormalaj.append(muntitaAlineo)


class LegStato(Enum):
    ATRIBUTOJ = -1
    ENHAVO = 0


###############################################################################
# Ĉi tie la programo legas la liniojn de la origina dosiero, procesas ilin,   #
# kaj fine donas la rezulton en formato kiun alia programo povos uzi por krei #
# la HTML-on de BEL Informas.                                                 #
###############################################################################

def generiBelInformas(dosierNomo = 'bel_informas.txt'):
    """Ĉefa funkcio por legi dosieron kaj doni strukturon uzeblan por krei HTML-on"""
    nunaAlineo = []
    kolumnoj = [[], []]
    larĝaj = []
    rezultoj = []
    stato = LegStato.ATRIBUTOJ
    atributoj = {
            'numero': 0,
            'dato': (0, 0, 0),
            'konvertiiksojn': False
    }

    with open(dosierNomo, 'r', encoding = 'utf-8') as fontaDosiero:
        k = 0
        for linio in fontaDosiero:
            if ĉuAtributo(linio):
                atrNomo, atrValoro = legiAtributon(linio)
                atributoj[atrNomo] = atrValoro
            elif stato is LegStato.ENHAVO and ĉuKolumnApartigilo(linio):
                aldoniAlineon(kolumnoj[k], larĝaj, nunaAlineo)
                nunaAlineo = []
                k = 1
            elif ĉuArtikolApartigilo(linio):
                if stato is LegStato.ATRIBUTOJ:
                    stato = LegStato.ENHAVO
                else:
                    aldoniAlineon(kolumnoj[k], larĝaj, nunaAlineo)
                    artikolRezulto = muntiArtikolon(kolumnoj[0], kolumnoj[1], larĝaj)
                    rezultoj.append(artikolRezulto)
                    kolumnoj = [[], []]
                    nunaAlineo = []
                    larĝaj = []
                    k = 0
            elif stato is LegStato.ENHAVO and ĉuAlineaApartigilo(linio):
                aldoniAlineon(kolumnoj[k], larĝaj, nunaAlineo)
                nunaAlineo = []
            elif stato is LegStato.ENHAVO:
                if atributoj['konvertiiksojn']:
                    linio = maliksigi(linio)
                nunaAlineo.append(linio)

        # Se la fonta dosiero finiĝas sen apartigilo de artikoloj, ni finos tion kion ni komencis:
        if kolumnoj[0] or kolumnoj[1] or larĝaj:
            aldoniAlineon(kolumnoj[k], larĝaj, nunaAlineo)
            artikolRezulto = muntiArtikolon(kolumnoj[0], kolumnoj[1], larĝaj)
            rezultoj.append(artikolRezulto)
    return rezultoj, atributoj['numero'], atributoj['dato']


if __name__ == '__main__':
    if len(sys.argv) >= 2:
        rezultoj = generiBelInformas(sys.argv[1])
    else:
        rezultoj = generiBelInformas()

    rezultDosierNomBazo = 'ero_de_bel_informas'
    with novaRezultDosiero(rezultDosierNomBazo, DosierTipo.UNIKODO) as (rezultDosierNomo, rezultDosiero):
        for artikolo in rezultoj:
            print(artikolo, ',', sep='', file=rezultDosiero)
        print('Mi skribis la rezulton en', rezultDosierNomo)
